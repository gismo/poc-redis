<?php
echo "Utilisation de Redis";

$redis = new Redis();
$redis->connect('redis', 6379);

$redis->ping("Erreur de connexion a la BD Redis");

$redis->set('key', 'La valeur de ma cle est bien enregistree !');
$retour = $redis->get('key');

echo  PHP_EOL . "Retour : \"$retour\"";